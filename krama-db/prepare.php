<?php

$raw = file('./raw.tsv');

$items = [];

array_shift($raw);

$currentDb = json_decode(file_get_contents('../db.json'), true);

function findCurrentDbItem ($brand) {
  global $currentDb;
  $brandLower = strtolower($brand);
  for ($i = 0; $i < count($currentDb); $i++) {
    $item = $currentDb[$i];
    if (strtolower($item['brand']) === $brandLower) {
      return $item;
    }
  }
  return null;
}

foreach ($raw as $line) {
  $l = explode("\t", $line);

  $brand = trim($l[0]);

  $item = findCurrentDbItem($brand);
  $color = trim($l[1]);
  $criterium = intval(trim($l[2]));
  if ($item) {
    $item["color"] = $color;
    $item["criterium"] = $criterium;
  } else {
    $item = [
      "brand" => $brand,
      "color" => $color,
      "criterium" => $criterium,
      "brandKeywords" => []
    ];
  }
  $items[] = $item;
}

file_put_contents('db.json', json_encode($items,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));