#!/bin/bash

rm -rf ./release/*
cp -rf background release
cp -rf browser-action release
cp -rf content-scripts release
cp -rf images release
cp -rf locales release
cp -rf manifest.json release

cd release

zip -r release.zip ./*