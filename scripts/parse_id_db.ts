import * as jsdom from 'jsdom'
import db from '../src/db'
import * as fs from 'fs'

const axios = require('axios')

const dbContents = require('../db.json')
const id_db = require('../id_db.json')
const clear_ids = require('../clear_ids.json')

const { JSDOM } = jsdom

const skipCategories = [
  'Товары для животных',
  'Электроника',
  'Бытовая техника',
  'Новогодние сувениры и украшения',
  'Спорт, туризм, отдых на природе',
  'Дача, сад, огород',
  'Одежда, галантерея, аксессуары',
  'Товары медицинского назначения',
  'Книги',
  'Канцелярские товары',
  'Товары для стройки и ремонта',
  'Автотовары',
  'Товары для кафе и ресторанов',
  'Лотереи, карты лояльности',
  'Бытовая химия, хозтовары',
  'Шеф-онлайн',
  'Красота, уход за собой',
  'Товары для дома'
]

const otherCountries = [
  'Франция',
  'Нидерланды',
  'Швейцария',
  'Польша',
  'Литва',
  'Латвия',
  'Китай',
  'Иран',
  'Турция',
  'Узбекистан',
  'Украина',
  'Италия',
  'Испания',
  'Израиль',
  'Казахстан',
  'Марокко',
  'Бельгия',
  'Перу',
  'Коста-Рика',
  'Египет',
  'Эквадор',
  'Молдова',
  'Южная Африка',
  'Германия',
  'Венгрия',
  'Тайланд',
  'Армения',
  'Абхазия',
  'Швеция',
  'Финляндия',
  'Норвегия',
  'Чили',
  'Болгария',
  'Румыния',
  'Объединенные Арабские Эмираты',
  'Корея',
  'Люксембург',
  'Македония',
  'Индия'
]

async function doWork () {
  await db.init(dbContents)

  const { data: mainPage } = await axios.get('https://e-dostavka.by')
  const { window } = new JSDOM(mainPage)
  const { document } = window

  const links = document.querySelectorAll('.catalog_menu > li > a')
  console.log('Parsed', links.length, 'categories')
  for (const link of Array.from(links)) {
    let url = link.getAttribute('href')

    if (skipCategories.indexOf(link.textContent) >= 0) {
      console.log('Skip category', link.textContent)
      continue
    }

    const res = await axios({
      method: 'get',
      url
    })
    url = res.request.res.responseUrl

    const checkedProducts: any = {}
    for (let i = 1;; i++) {
      let page = Math.ceil(i / 12)
      let steep = (i - 1) % 12 + 1

      console.log('Analyzing category', link.textContent, url, page, steep)
      const urlToFetch = url + '?page=' + page + '&_=' + Date.now() + '&lazy_steep=' + steep
      console.log('Loading url', urlToFetch)

      const { data: html } = await axios({
        method: 'get',
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Referer': url + '?page=' + page
        },
        url: urlToFetch
      })
      const { window: windowProducts } = new JSDOM(html)
      const { document: documentProducts } = windowProducts

      const products = documentProducts.querySelectorAll('.products_card')
      if (!products.length) {
        break
      }
      console.log('Found products', products.length, 'checked products', Object.keys(checkedProducts).length)
      let foundNotCheckedProduct = false
      for (const product of Array.from(products)) {
        const country = product.querySelector('.small_country')
        const productId = product.querySelector('input[name="product_id"]').getAttribute('value')

        if (!productId) {
          console.log('no product id')
          continue
        }
        if (checkedProducts[productId]) {
          continue
        }
        checkedProducts[productId] = true
        foundNotCheckedProduct = true

        if (country && otherCountries.includes(country.textContent.trim())) {
          console.log(productId, 'country in the white list')
          continue
        }
        if (country) {
          console.log('Country', country.textContent)
        }
        // if (clear_ids.includes(productId)) {
        //   console.log('Clear', productId)
        //   continue
        // }
        console.log('Check product', productId)
        const productUrl = 'https://e-dostavka.by/catalog/item_' + productId + '.html'
        let productInfo = null
        const cacheName = 'prod_' + productId + '.cache'
        try {
          const cached = fs.readFileSync('./cache/' + cacheName).toString('utf8')
          if (cached) {
            productInfo = cached
          }
        } catch (ex) {

        }
        if (!productInfo) {
          const { data: tmp } = await axios({
            method: 'get',
            url: productUrl,
            headers: {
              'X-Requested-With': 'XMLHttpRequest'
            }
          })
          productInfo = tmp
          fs.writeFileSync('./cache/' + cacheName, productInfo)
        }
        const { window: windowProduct } = new JSDOM(productInfo)
        const { document: documentProduct } = windowProduct
        const manufacturer = documentProduct.querySelector('.property_8563 .value')
        if (!manufacturer) {
          console.log('could not find the manufacturer')
          continue
        }
        const brandMatched: any = db.matchBrand(manufacturer.textContent)
        if (!brandMatched) {
          console.log('manufacturer is clear', manufacturer.textContent)
          clear_ids.push(productId)
          fs.writeFileSync(__dirname + '/../clear_ids.json', JSON.stringify(clear_ids))
          continue
        }
        id_db.items[productId] = {
          brand: brandMatched.brand,
          color: brandMatched.color,
          criterium: brandMatched.criterium
        }
        fs.writeFileSync(__dirname + '/../id_db.json', JSON.stringify(id_db, null, 2))
        console.log('FOUND!', brandMatched)
      }
      if (!foundNotCheckedProduct) {
        console.log('All products where already checked, aborting')
        break
      }
    }
  }
}

doWork()