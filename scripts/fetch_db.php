<?php

$raw = file_get_contents('https://docs.google.com/spreadsheets/d/1kAC9nUiIJP1qY2esPGI_wYWZ40vL7ChvEl4BmH9qF6k/gviz/tq?sheet=list');

if(!preg_match('@google.visualization.Query.setResponse\((.+?)\);@', $raw, $m)) {
  die('Failed to parse');
}

function rowToArr ($row) {
  $res = [];
  foreach ($row['c'] as $c) {
    if ($c) {
      $res[] = $c['v'];
    } else {
      $res[] = null;
    }
  }

  return $res;
}

$raw = json_decode($m[1], true);

if (empty($raw['table']['rows'])) {
  die('Could not find the rows');
}

$rows = $raw['table']['rows'];

$header = rowToArr(array_shift($rows));

if ($header !== ['Бренд', 'Категория', 'Описание', 'Логотип', 'Ключевые слова']) {
  die('Please check the source header, because it does not match the required one. DB has changed?');
}

$data = [];
foreach ($rows as $row) {
  $r = rowToArr($row);
  $data[] = [
    'brand' => $r[0],
    'category' => $r[1],
    'description' => $r[2],
    'keywords' => preg_split('@\s*,\s*@', trim($r[4]))
  ];
}

file_put_contents('../db.json', json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

print "DB updated successfully!\n";