import './index.scss';

document.addEventListener('DOMContentLoaded', () => {
  chrome.storage.local.get('belTexts', data => {
    const node: any = document.querySelector('#bel-texts')
    if (data.belTexts !== false) {
      node.checked = true
    }
  })

  document.querySelector('#bel-texts').addEventListener('change', event => {
    const node: any = event.target
    chrome.storage.local.set({
      belTexts: node.checked
    })
  })
})