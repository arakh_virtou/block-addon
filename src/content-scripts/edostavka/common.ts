import { DBRecord, DB } from '../../db'
import tippy from 'tippy.js'
import i18n from '../i18n'

const ITEM_PROCESSED_CLASS = 'block-addon-processed'
const ITEM_BANNED_CLASS = 'block-addon-banned'

export function findAndProcessItems (db: DB) {
  const items: any[] = Array.from(document.querySelectorAll('[name="product_id"]'))
  for (const item of items) {
    const itemRoot: any = item.parentNode.parentNode
    if (itemRoot.classList.contains(ITEM_PROCESSED_CLASS)) {
      continue
    }
    itemRoot.classList.add(ITEM_PROCESSED_CLASS)

    const productId = item.value
    const bannedInfo = db.getById(productId)
    if (bannedInfo) {
      itemRoot.classList.add(ITEM_BANNED_CLASS)
      if (bannedInfo.color) {
        itemRoot.classList.add(ITEM_BANNED_CLASS + '-' + bannedInfo.color.toLowerCase())
      }
      const btn = itemRoot.querySelector('.prices__wrapper .form_elements button')
      let message = i18n.getMessage('criterium_' + bannedInfo.criterium + '_message')
      if (!message) {
        message = 'Покупая продукты "' + bannedInfo.brand + '" вы оказываете поддержку режиму'
      }
      message = message.replace('{companyName}', bannedInfo.brand)
      const res = tippy(btn, {
        content: message
      })
    } else {
    }
  }
}