/**
 * Общий обработчик, который обрабатывает списки продуктов, в том числе и основной список
 */

import { DBRecord, DB } from '../../db'
import { findAndProcessItems } from './common'

const PRODUCTS_MAIN_LIST_SELECTOR = '.products_catalog .products_block__wrapper'

let mutationObserver: MutationObserver = null

function setMutationObserver (db: DB) {
  if (mutationObserver) {
    mutationObserver.disconnect()
  }
  mutationObserver = new MutationObserver(mutations => {
    findAndProcessItems(db)
  })

  const node = document.querySelector(PRODUCTS_MAIN_LIST_SELECTOR)
  mutationObserver.observe(node, {
    childList: true
  })
}

export async function process (db: DB) {
  findAndProcessItems(db)
  setMutationObserver(db)
}