import { waitForNode } from '../../util/dom'
import { DBRecord, DB } from '../../db'
import i18n from '../i18n'

const PRODUCT_CONTAINER_SELECTOR = '.mfp-container'
const PRODUCT_PARAMETERS_TAB_SELECTOR = PRODUCT_CONTAINER_SELECTOR + ' .tab_content.product_parameters'
const FORM_ELEMENTS_SELECTOR = PRODUCT_CONTAINER_SELECTOR + ' .services_wrap .form_elements'

const urlRegexp = /\/catalog\/item_[0-9]+\.html$/

export function isItemPage (url: string) {
  return urlRegexp.test(url)
}

export async function process (db: DB): Promise<DBRecord> {
  await waitForNode(PRODUCT_PARAMETERS_TAB_SELECTOR)

  const manufacturerNode = document.querySelector(PRODUCT_PARAMETERS_TAB_SELECTOR + ' .property_8563 .value')
  if (!manufacturerNode) {
    return null
  }

  const productInput: any = document.querySelector(PRODUCT_CONTAINER_SELECTOR + ' input[name="product_id"]')
  const productId = productInput.value
  const bannedInfo = db.getById(productId)

  if (bannedInfo) {
    const formElements = document.querySelector(FORM_ELEMENTS_SELECTOR)
    formElements.classList.add('block-addon-danger')

    if (bannedInfo.color) {
      formElements.classList.add('block-addon-danger-' + bannedInfo.color.toLowerCase())
    }

    const button = document.querySelector(FORM_ELEMENTS_SELECTOR + ' button')
    button.classList.add('block-addon-buy-danger')

    const warning = document.createElement('div')
    warning.classList.add('block-addon-product-warning')
    let message = i18n.getMessage('criterium_' + bannedInfo.criterium + '_message')
    if (!message) {
      message = 'Покупая продукты "' + bannedInfo.brand + '" вы оказываете поддержку режиму'
    }
    message = message.replace('{companyName}', bannedInfo.brand)
    warning.innerHTML = message
    document.querySelector(FORM_ELEMENTS_SELECTOR).appendChild(warning)
  }

  return bannedInfo
}