import db from '../db'
import { isItemPage, process as processItemPage } from './edostavka/itempage'
import { process as processAnyPage } from './edostavka/anypage'
import { process as processSearch } from './edostavka/search'
import i18n from './i18n'
import './edostavka/edostavka.scss';

function processPage () {
  const url = document.location.href
  if (isItemPage(url)) {
    processItemPage(db)
  } else {
    processAnyPage(db)
  }
  processSearch(db)
}

async function start (dbData: any) {
  await db.init(null, dbData)

  window.addEventListener('popstate', () => {
    processPage()
  })

  processPage()
}

chrome.runtime.sendMessage({
  type: 'contentScriptInitialized'
}, data => {
  console.log('GOT', data)
  i18n.setMessages(data.messages)
  start(data.db)
})