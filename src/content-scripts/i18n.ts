class I18n {
  private messages: any
  setMessages (messages: any) {
    this.messages = messages
  }
  getMessage (name: string) {
    return this.messages[name].message
  }
}

export default new I18n