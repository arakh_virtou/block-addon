import * as db from '../../id_db.json'
import * as ruMessages from '../../locales/ru/messages.json'
import * as byMessages from '../../locales/by/messages.json'

console.log('Loaded bg page')

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  chrome.storage.local.get('belTexts', ({ belTexts }) => {
    let messages = byMessages
    console.log('TEXTS', belTexts)
    if (belTexts === false) {
      messages = ruMessages
    }
    sendResponse({
      type: 'initData',
      messages,
      db
    })
  })

  return true
})
