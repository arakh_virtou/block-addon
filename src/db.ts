export interface DBRecord {
  brand: string
  category: string
  description: string
  keywords: string[]
  brandKeywords?: string[]
}

export class DB {
  private db: DBRecord[]
  private dbId: any

  public async init (contents: DBRecord[], dbId?: any) {
    this.db = contents
    this.dbId = dbId
  }

  public getById (id: string) {
    return this.dbId.items[id]
  }

  public matchBrand (brand: string): DBRecord {
    const inputLowerBrand = brand.toLowerCase()
    for (const dbItem of this.db) {
      const lowerBrand = dbItem.brand.toLowerCase()
      if (inputLowerBrand.indexOf(lowerBrand) >= 0) {
        console.log('MATCH brand!', inputLowerBrand, lowerBrand)
        return dbItem
      }
      if (dbItem.brandKeywords) {
        for (const brandKeyword of dbItem.brandKeywords) {
          if (inputLowerBrand.indexOf(brandKeyword) >= 0) {
            console.log('MATCH brand keyword!', inputLowerBrand, brandKeyword)
            return dbItem
          }
        }
      }
    }
    return null
  }
}

export default new DB()