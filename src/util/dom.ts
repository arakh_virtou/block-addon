import { wait } from './misc'

export async function waitForNode (selector: string, timeout: number = 5e3) {
  const check = () => document.querySelector(selector)
  if (check()) {
    return true
  }
  let spent = 0
  const iterationTimeout = 100
  while (spent < timeout) {
    await wait(iterationTimeout)
    spent += iterationTimeout

    if (check()) {
      return true
    }
  }

  return false
}