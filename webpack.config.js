const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const tsRule = {
  test: /\.tsx?$/,
  use: 'ts-loader',
  exclude: /node_modules/,
}
const resolveExtensions = [ '.tsx', '.ts', '.js' ]

const baseConfig = {
  module: {
    rules: [
      tsRule,
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      }
    ],
  },
  resolve: {
    extensions: resolveExtensions
  }
}

const baseConfigWithCssExtract = ({ cssName }) => {
  return {
    plugins: [new MiniCssExtractPlugin({
      filename: cssName
    })],
    module: {
      rules: [
        tsRule,
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            // Creates `style` nodes from JS strings
            //'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            // Compiles Sass to CSS
            'sass-loader',
          ],
        }
      ],
    },
    resolve: {
      extensions: resolveExtensions
    }
  }
}

module.exports = [
  Object.assign({
    entry: './src/content-scripts/edostavka.ts',
    output: {
      filename: 'edostavka.js',
      path: path.resolve(__dirname, 'content-scripts'),
    },
  }, baseConfigWithCssExtract({ cssName: 'edostavka.css' })),

  Object.assign({
    entry: './src/browser-action/index.ts',
    output: {
      filename: 'script.js',
      path: path.resolve(__dirname, 'browser-action'),
    },
  }, baseConfig),

  Object.assign({
    entry: './src/background/index.ts',
    output: {
      filename: 'script.js',
      path: path.resolve(__dirname, 'background'),
    },
  }, baseConfig)
]
